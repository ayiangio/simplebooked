const express = require('express');
const app = express();
const port = process.env.PORT || 3300;
const bodyParser = require('body-parser');
const logger = require('morgan');
const routeUser = require('./src/route/user');
const routeClass = require('./src/route/class');
app.use(
    bodyParser.urlencoded({
        extended :false
    })
)
app.listen(port);
app.use(logger('dev'))
console.log('"Server Is Running On Port', port);
routeUser(app);
routeClass(app);