# Restfull API Express

<p align="center">
  <a href="https://nodejs.org/">
    <img src="https://cdn-images-1.medium.com/max/871/1*d2zLEjERsrs1Rzk_95QU9A.png">
  </a>
</p>

## Prerequiste

- Node.js - Download and install [Node.js](https://nodejs.org/en/)
- Mysql - Download and install [Mysql](https://www.mysql.com/downloads/)

## Steps to run the app on your machine 
1. Open CMD or Terminal and enter to the app directory
2. [Instalation](#instalation)
3. Make a new file called **.env** in the root directory, set up first [here](#create-environment)
4. Turn on Web Server and MySQL can using Third-party tool like xampp, etc.
5. Create Database on **phpmyadmin**
6. Open Postman desktop application or Chrome web app extension that has installed before
7. Choose HTTP Method and enter request url.(ex. localhost:3300)
8. You can see the Collection[![Run in Postman](https://run.pstmn.io/button.svg)](https://www.getpostman.com/collections/790705b64c47fc1170ef)

## Instalation

```
$ https://gitlab.com/ayiangio/simplebooked.git
$ cd simpleBooked
$ npm install
```
## Create Environment
```
- Rename File .env.local to .env
- Create Database Name dgt and import the data 
```
## Start Server
```
$ npm start
```

## Contributors
<center>
  <table>
    <tr>
      <td align="center">
        <a href="https://github.com/ayiangio">
          <img width="100" src="https://avatars3.githubusercontent.com/u/15377357?s=460&v=4" alt="Ayiangio"><br/>
          <sub><b>Ayi Angio</b></sub>
        </a>
      </td>
    </tr>
  </table>
</center>