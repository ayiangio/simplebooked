const classBooked = require('../models/class');
const respon = require('../helper/response');
const jwt = require('jsonwebtoken')

module.exports = {
    getAllClass: (req, res) => {
        
        classBooked.getAllClass()
            .then((result) => {
                respon.response(res, result, 200)
            })
            .catch((err) => {
                // console.log(err)
                return respon.response(res, null, 404, "username Not Avaliable !!!")
            })
    },
    
}