const user = require('../models/user');
const respon = require('../helper/response');
const jwt = require('jsonwebtoken')

module.exports = {
    register: (req, res) => {
        const salt = respon.getRandomSalt(20)
        const passHash = respon.setPass(req.body.password, salt)

        const data = {
            username: req.body.username,
            password: passHash.passHash,
            salt: passHash.salt,
        }
        user.register(data)
            .then((resultUser) => {
                console.log(resultUser)
                respon.response(res, resultUser, 200)
            })
            .catch((err) => {
                console.log(err)
                return respon.response(res, null, 404, "username Not Avaliable !!!")
            })
    },
    login: (req, res) => {
        const username = req.body.username
        const pass = req.body.password
        user.getByUserName(username)
            .then((result) => {
                const dataUser = result[0]
                const userPass = respon.setPass(pass, dataUser.salt).passHash

                if (userPass === dataUser.password) {
                    dataUser.token = jwt.sign({
                        id_user: dataUser.id_user
                    }, process.env.SECRET_KEY, {
                        expiresIn: '120m'
                    })

                    delete dataUser.salt
                    delete dataUser.password
                    return respon.response(res, dataUser, 200)
                } else {
                    return respon.response(res, null, 403, "Wrong Password !!!")
                }
            })
            .catch((err) => {
                return respon.response(res, null, 403, "username Not Register !!!")
            })
    }
}