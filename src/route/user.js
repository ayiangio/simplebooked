module.exports = (app) => {
    const user = require('../controllers/user');
    const auth = require('../helper/auth')
    app
        .post('/register', user.register)
        .post('/login',auth.authInfo, user.login)
}