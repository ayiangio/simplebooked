module.exports = (app) => {
    const classBooked = require('../controllers/class');
    const auth = require('../helper/auth')
    app
        .get('/dataclass',auth.authInfo,auth.accessToken, classBooked.getAllClass)
}