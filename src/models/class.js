const connection = require('../connection/db');

module.exports = {
    getAllClass: () => {
        return new Promise((resolve, reject) => {
            connection.query('SELECT * FROM class', (err, result) => {
                if (!err) {
                    resolve(result)
                } else {
                    reject(new Error(err))
                }
            })

        })
    },
}